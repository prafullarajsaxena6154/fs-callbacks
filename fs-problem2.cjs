const fs = require('fs');

function fsProblem2(userFileName) {                                  //start function, takes the file path from the test file and processes;
    fs.readFile(userFileName, CB = (err, data) => {
        if (err) {
            console.log(err);
        }
        else {
            fs.writeFile('lipsumUC.txt', (data.toString()).toUpperCase(), (err) => {            //writing into new file UpperCased;
                if (err) {
                    console.log(err);
                }
                else {
                    console.log("File successfully Upper-Cased.");
                    fs.writeFile('filenames.txt', 'lipsumUC.txt' + '\n', (err) => {             //writing file name into a new file;
                        if (err) {
                            console.log(err);
                        }
                        else {
                            console.log("File Name lipsumUC.txt appended.");
                            fs.readFile('lipsumUC.txt', CB = (err, data) => {                   //reading from a previously created file for new data;
                                if (err) {
                                    console.log(err);
                                }
                                else {
                                    data = data.toString().toLowerCase();
                                    data = data.split(/[.\n]+/);

                                    fs.writeFile('lipsumLCSentences.txt', JSON.stringify(data), (err) => {                              //writing new data in a new file;
                                        if (err) {
                                            console.log(err);
                                        }
                                        else {
                                            console.log("File successfully lower-Cased and stored in sentences form in an array.");
                                            fs.appendFile('filenames.txt', 'lipsumLCSentences.txt\n', (err) => {                        //adding new file name in file;
                                                if (err) {
                                                    console.log(err);
                                                }
                                                else {
                                                    console.log("File name lipsumLCSentences appended successfully.");
                                                    fs.readFile('lipsumLCSentences.txt', CB2 = (err2, data2) => {                       //reading from a previously created file for new data;
                                                        if (err2) {
                                                            console.log(err2);
                                                        }
                                                        else {
                                                            data2 = JSON.parse(data2);
                                                            data2 = data2.sort();
                                                            fs.writeFile('lipsumLCSentencesSorted.txt', data2, (err) => {               //writing new data in a new file;
                                                                if (err) {
                                                                    console.log(err);
                                                                }
                                                                else {
                                                                    fs.appendFile('filenames.txt', 'lipsumLCSentencesSorted.txt', (err) => {                    //adding new file name in file;
                                                                        if (err) {
                                                                            console.log(err);
                                                                        }
                                                                        else {
                                                                            console.log("File name lipsumLCSentencesSorted appended successfully.");
                                                                            fs.readFile('filenames.txt', CB = (err3, data3) => {                                //reading from a previously created file for new data;
                                                                                if (err3) {
                                                                                    console.log(err3);
                                                                                }
                                                                                else {
                                                                                    let fileNames = data3.toString().split('\n');
                                                                                    fileNames.map((ele) => {
                                                                                        fs.unlink(ele, (err4) => {                                              //deleting the files simutaneously;
                                                                                            if (err4) {
                                                                                                console.log(err4);
                                                                                            }
                                                                                            else {
                                                                                                console.log(ele + ' deleted!');
                                                                                            }
                                                                                        })
                                                                                    })
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })
};

module.exports = fsProblem2;


