// const { create } = require('domain');
const fs = require('fs');
const path = require('path');

function fsProblem1(directoryPath, randomNumberOfFiles) {                                                   //function to create directory
    fs.mkdir(directoryPath, (err) => {
        if (err) {
            throw err;
        }
        else {
            console.log("Directory callbacks created successfully")
            createRandomFile(randomNumberOfFiles, directoryPath);
        }
    });
}
function createRandomFile(numFile, directoryPath) {                                                         //function to create random new files, with random content
    let fileNames = [];
    for (let index = 0; index < numFile; index++) {
        const characters = "ABCMNOPQRSThfgdj3456789";
        let random_Content = '';

        for (let index = 0; index <= Math.random() * characters.length; index++) {
            random_Content = (characters.substring(Math.random() * characters.length, characters.length + 2));
        }
        let fileName = "file_" + index;
        fs.writeFile(`${directoryPath}/${fileName}`, JSON.stringify(random_Content, null, 10), function (err) {
            if (err) {
                console.log(err);
            }
            else {
                fileNames.push(fileName);
                if (fileNames.length == numFile) {
                    deleteFiles(fileNames,directoryPath);
                }
                console.log(`File ${fileName} saved successfully`)
            }
        });
    };
}

function deleteFiles(fileList, directoryPath) {                                                              //function to delete files from read directory
    for(let index = 0 ; index<fileList.length; index++)
        {
        let filePath = path.join(directoryPath, fileList[index]);
        fs.unlink(filePath, function (err) {
            if (err) {
                console.log(err);
            } else {
                console.log("deleted sucessfully " + filePath);
            }
        })
    }

}
module.exports = fsProblem1;

